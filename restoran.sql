-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2019 at 12:31 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restoran`
--

-- --------------------------------------------------------

--
-- Table structure for table `jeniskelamin`
--

CREATE TABLE `jeniskelamin` (
  `idjenis` int(11) NOT NULL,
  `jeniskelamin` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jeniskelamin`
--

INSERT INTO `jeniskelamin` (`idjenis`, `jeniskelamin`) VALUES
(1, 'Laki-Laki'),
(2, 'Perempuan');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `idlevel` int(11) NOT NULL,
  `namalevel` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`idlevel`, `namalevel`) VALUES
(1, 'Administrator'),
(2, 'Waiter'),
(3, 'Kasir'),
(4, 'Owner'),
(5, 'Superuser');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `idmenu` varchar(11) NOT NULL,
  `namamenu` text NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`idmenu`, `namamenu`, `harga`) VALUES
('M002', 'Kakap Merah', 25000),
('M003', 'qweeqwe', 35000),
('M004', 'Oreo', 15000),
('M005', 'Ayam Geprek + Nasi', 15000),
('M006', 'Softdrink', 7000),
('M007', 'Teh Manis', 5000);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `idpelanggan` varchar(11) NOT NULL,
  `namapelanggan` text NOT NULL,
  `jeniskelamin` varchar(11) NOT NULL,
  `nohp` varchar(13) NOT NULL,
  `alamat` varchar(95) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`idpelanggan`, `namapelanggan`, `jeniskelamin`, `nohp`, `alamat`, `tanggal`) VALUES
('P002', 'Jodi Pascal', '1', '089947378809', 'Rancaekek, Desa Jelegong No.18', '2019-02-09'),
('P003', 'Zaki Albani', '1', '0978664456', 'Rancaekek, Desa Jelegong No.18', '2019-02-11'),
('P006', 'Jodi Pascal', '1', '0899473889', 'Rancaekek, Desa Jelegong No.18', '2019-02-11'),
('P007', 'test123', '1', '6134122212', 'Rancaekek, Desa Jelegong No.18', '2019-02-21'),
('P008', 'Jodi Pascal', '1', '0898978880', 'Bandung', '2019-02-22'),
('P009', 'Zaki Albani', '1', '0899283788', 'Bandung', '2019-02-22'),
('P010', 'Jodi Pascal', '1', '07895745633', 'Bandung', '2019-03-01'),
('P011', 'Jodi Pascal', '1', '0899478989', 'Rancaekek, Desa Jelegong No.18', '2019-03-12'),
('P012', 'Zaki Albani', '1', '0889954156', 'Bandung', '2019-03-12'),
('P013', 'Jodi Pascal', '1', '08994763565', 'Bandung, Rancaekek', '2019-03-14'),
('P014', 'Jodi Pascal', '1', '08994737880', 'Bandung', '2019-03-20'),
('P015', 'Jodi Pascal', '1', '8994763565', 'Bandung, Rancaekek', '2019-03-21'),
('P016', 'Jodi Pascal', '1', '08994737880', 'Rancaekek, Desa Jelegong No.18', '2019-03-22'),
('P017', 'Jodi Pascal', '1', '0892345234', 'Rancaekek, Desa Jelegong No.18', '2019-03-23'),
('P018', 'jodi', '1', '0809898989', 'asgdysa', '2019-03-23'),
('P019', 'Jodi Pascal', '1', '07345632452', 'Rancaekek, Desa Jelegong No.18', '2019-03-26'),
('P020', 'Jodi Pascal', '1', '0899473789', 'Rancaekek, Desa Jelegong No.18', '2019-03-29'),
('P021', 'Jodi Pascal', '1', '9789821938', 'Rancaekek, Desa Jelegong No.18', '2019-03-29'),
('P022', 'Jodi Pascal', '1', '07896587434', 'Rancaekek, Desa Jelegong No.18', '2019-03-30'),
('P023', 'Jodi Pascal', '1', '097453553343', 'Rancaekek, Desa Jelegong No.18', '2019-03-31');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan`
--

CREATE TABLE `pesanan` (
  `idpesanan` varchar(11) NOT NULL,
  `idmenu` varchar(11) NOT NULL,
  `idpelanggan` varchar(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `iduser` varchar(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesanan`
--

INSERT INTO `pesanan` (`idpesanan`, `idmenu`, `idpelanggan`, `harga`, `iduser`, `jumlah`) VALUES
('0', '0', 'P008', 25000, '0', 3),
('O001', 'M004', 'P008', 15000, 'Jodi Pascal', 5),
('O001', 'M002', 'P008', 25000, 'Jodi Pascal', 1),
('O001', 'M001', 'P008', 15000, 'Jodi Pascal', 3),
('O002', 'M003', 'P008', 35000, 'Jodi Pascal', 2),
('O002', 'M002', 'P008', 25000, 'Jodi Pascal', 1),
('O002', 'M004', 'P008', 15000, 'Jodi Pascal', 4),
('O003', 'M004', 'P008', 15000, 'Jodi Pascal', 3),
('O003', 'M001', 'P008', 15000, 'Jodi Pascal', 2),
('O004', 'M002', 'P009', 25000, 'Jodi Pascal', 3),
('O004', 'M001', 'P009', 15000, 'Jodi Pascal', 2),
('O004', 'M004', 'P009', 15000, 'Jodi Pascal', 2),
('O005', 'M002', 'P008', 25000, 'Jodi Pascal', 7),
('O006', 'M002', 'P009', 25000, 'Jodi Pascal', 3),
('O006', 'M001', 'P009', 15000, 'Jodi Pascal', 3),
('O006', 'M004', 'P009', 15000, 'Jodi Pascal', 4),
('O007', 'M002', 'P009', 25000, 'Jodi Pascal', 3),
('O007', 'M001', 'P009', 15000, 'Jodi Pascal', 4),
('O007', 'M004', 'P009', 15000, 'Jodi Pascal', 2),
('O008', 'M002', 'P009', 25000, 'Jodi Pascal', 3),
('O009', 'M002', 'P009', 25000, 'Jodi Pascal', 3),
('O010', 'M003', 'P009', 35000, 'Jodi Pascal', 3),
('O011', 'M004', 'P010', 15000, 'Jodi Pascal', 3),
('O011', 'M002', 'P010', 25000, 'Jodi Pascal', 3),
('O012', 'M004', 'P010', 15000, 'Jodi Pascal', 3),
('O013', 'M002', 'P010', 25000, 'Jodi Pascal', 2),
('O014', 'M002', 'P010', 25000, 'Jodi Pascal', 3),
('O015', 'M002', 'P011', 25000, 'Jodi Pascal', 3),
('O016', 'M002', 'P012', 25000, 'Jodi Pascal', 5),
('O017', 'M002', 'P013', 25000, 'Jodi Pascal', 3),
('O018', 'M002', 'P014', 25000, 'Renaldi', 3),
('O018', 'M004', 'P014', 15000, 'Renaldi', 2),
('O019', 'M002', 'P015', 25000, 'Renaldi', 3),
('O019', 'M004', 'P015', 15000, 'Renaldi', 2),
('O020', 'M003', 'P016', 35000, 'Renaldi', 2),
('O020', 'M002', 'P016', 25000, 'Renaldi', 1),
('O021', 'M004', 'P017', 15000, 'Renaldi', 3),
('O022', 'M003', 'P019', 35000, 'Jodi Pascal', 3),
('O023', 'M002', 'P019', 25000, 'Jodi Pascal', 4),
('O024', 'M003', 'P022', 35000, 'Jodi Pascal', 3),
('O024', 'M002', 'P022', 25000, 'Jodi Pascal', 4),
('O024', 'M004', 'P022', 15000, 'Jodi Pascal', 4),
('O025', 'M004', 'P023', 15000, 'Jodi Pascal', 3),
('O026', 'M004', 'P023', 15000, 'Jodi Pascal', 3),
('O026', 'M002', 'P023', 25000, 'Jodi Pascal', 4),
('O027', 'M006', 'P023', 7000, 'Jodi Pascal', 3),
('O027', 'M007', 'P023', 5000, 'Jodi Pascal', 4);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `idtransaksi` varchar(11) NOT NULL,
  `idpesanan` varchar(11) NOT NULL,
  `total` int(11) NOT NULL,
  `bayar` int(11) NOT NULL,
  `kembali` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`idtransaksi`, `idpesanan`, `total`, `bayar`, `kembali`, `tanggal`, `status`) VALUES
('T010', 'O001', 145000, 0, 0, '0000-00-00', ''),
('T011', 'O002', 155000, 0, 0, '0000-00-00', ''),
('T012', 'O003', 75000, 0, 0, '2019-02-21', ''),
('T014', 'O004', 135000, 0, 0, '2019-02-22', ''),
('T015', 'O005', 175000, 0, 0, '2019-02-22', ''),
('T016', 'O006', 180000, 0, 0, '2019-02-22', ''),
('T017', 'O007', 165000, 0, 0, '2019-02-22', ''),
('T018', 'O008', 75000, 0, 0, '2019-02-22', ''),
('T019', 'O009', 75000, 0, 0, '2019-02-22', ''),
('T020', 'O010', 105000, 0, 0, '2019-02-22', ''),
('T021', 'O011', 120000, 0, 0, '2019-03-01', ''),
('T022', 'O012', 45000, 55000, 10000, '2019-03-01', 'done'),
('T023', 'O013', 50000, 100000, 50000, '2019-03-01', 'done'),
('T024', 'O014', 75000, 100000, 25000, '2019-03-01', 'DONE'),
('T025', 'O015', 75000, 100000, 25000, '2019-03-12', 'DONE'),
('T026', 'O016', 125000, 150000, 25000, '2019-03-12', 'DONE'),
('T027', 'O017', 75000, 100000, 25000, '2019-03-14', 'DONE'),
('T028', 'O018', 105000, 110000, 5000, '2019-03-20', 'DONE'),
('T029', 'O019', 105000, 110000, 5000, '2019-03-21', 'DONE'),
('T030', 'O020', 95000, 100000, 5000, '2019-03-22', 'DONE'),
('T031', 'O021', 45000, 50000, 5000, '2019-03-30', 'DONE'),
('T032', 'O022', 0, 0, 0, '2019-03-23', 'Belum Bayar'),
('T033', 'O022', 105000, 0, 0, '2019-03-26', 'Belum Bayar'),
('T034', 'O023', 100000, 150000, 50000, '2019-03-30', 'DONE'),
('T035', 'O024', 265000, 300000, 35000, '2019-03-30', 'DONE'),
('T036', 'O025', 45000, 50000, 5000, '2019-03-31', 'DONE'),
('T037', 'O026', 145000, 150000, 5000, '2019-03-31', 'DONE'),
('T038', 'O027', 41000, 50000, 9000, '2019-03-31', 'DONE');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `namauser` text NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` text NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `nohp` varchar(13) NOT NULL,
  `email` text NOT NULL,
  `leveluser` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iduser`, `namauser`, `username`, `password`, `alamat`, `nohp`, `email`, `leveluser`) VALUES
(1, 'Jodi Pascal', 'admin', '0192023a7bbd73250516f069df18b500', 'Bandung, Rancaekek', '08994737880', 'jodipascal722@gmail.com', '1'),
(2, 'Renaldi', 'waiter', 'e82d611b52164e7474fd1f3b6d2c68db', 'Bandung, Cipacing', '08921378', 'renaldi@123.com', '2'),
(3, 'Arya', 'kasir', 'de28f8f7998f23ab4194b51a6029416f', 'Bandung, Cileunyi', '09812398', 'aryaw@www.com', '3'),
(4, 'owner', 'owner', '5be057accb25758101fa5eadbbd79503', 'owner', '08989992', 'owner@own.com', '4'),
(5, 'Jodi Pascal', 'jodi', '7815696ecbf1c96e6894b779456d330e', 'Bandung, Rancaekek', '08994737880', 'jodipascal722@gmail.com', '1'),
(6, 'Juvenile Delinquency', 'moriksa', '353eaaa3f2c33d53d97457b84185da2c', 'Rancaekek, Desa Jelegong No.18', '08994737880', 'jodipascal722@gmail.com', '5');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jeniskelamin`
--
ALTER TABLE `jeniskelamin`
  ADD PRIMARY KEY (`idjenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`idlevel`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idmenu`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`idpelanggan`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`idtransaksi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jeniskelamin`
--
ALTER TABLE `jeniskelamin`
  MODIFY `idjenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `idlevel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
