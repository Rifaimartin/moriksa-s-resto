
<body>
    <h1 class="title-agile text-center">Login Moriksa's Resto</h1>
    <div class="content-w3ls">
        <div class="content-bottom">
			<h2>Login</h2>
            <?php echo form_open("oto/login"); ?>
                    <form action="" method="post">
                <div class="field-group">
                    <span class="fa fa-user" aria-hidden="true"></span>
                    <div class="wthree-field">
                        <input name="username" id="username" type="text" value="" placeholder="e.g MoRiksa" required>
                    </div>
                </div>
                <div class="field-group">
                    <span class="fa fa-lock" aria-hidden="true"></span>
                    <div class="wthree-field">
                        <input name="password" id="password" type="Password" placeholder="Password">
                    </div>
                </div>
                <div class="field-group">
                    <div class="check">
                        <label class="checkbox w3l">
                            <input type="checkbox" onclick="myFunction()">
                            <i> </i>show password</label>
                    </div>
                    <!-- script for show password -->
                    <script>
                        function myFunction() {
                            var x = document.getElementById("password");
                            if (x.type === "password") {
                                x.type = "text";
                            } else {
                                x.type = "password";
                            }
                        }
                    </script>
                    <!-- //script for show password -->
                </div>
                <div class="wthree-field">
                    <input id="btn-login" name="btn-login" type="submit" value="login" />
                </div>
                <ul class="list-login">
                    <li class="switch-agileits" >
                        
                    </li>
                    <li>
                        <a href="#" class="text-right">Lupa password?</a>
                    </li>
                    <li class="clearfix"></li>
                </ul>
            </form>
            <?php echo form_close(); ?>
        </div>
    </div>
</body>
<!-- //Body -->
</html>
