<?php

error_reporting(0);


date_default_timezone_set('Asia/Jakarta');// change according timezone
$currentTime = date( 'd-m-Y A', time () );

foreach ($ambil->result() as $data) {

$pdf = new FPDF("P","mm","A4");


$pdf->AddPage();

$pdf->SetFont('Arial','B',14);


$pdf->Cell(130,5,'MoRiksa Restaurant',0,0);
$pdf->Cell(59,5,'Transaction',0,1);

$pdf->SetFont('Arial','',12);

$pdf->Cell(130,5,'Baknus666 Street',0,0);
$pdf->Cell(59,5,'',0,1);

$pdf->Cell(130,5,'Bandung, Indonesia (4034)',0,0);
$pdf->Cell(25,5,'Date',0,0);
$pdf->Cell(34,5,$currentTime,0,1);

$pdf->Cell(130,5,'Phone [+62 8994737880]',0,0);
$pdf->Cell(25,5,'Transaction',0,0);
$pdf->Cell(34,5,$data->idtransaksi,0,1);

$pdf->Cell(130,5,'Fax [+666]',0,0);
$pdf->Cell(25,5,'Customer ID',0,0);
$pdf->Cell(34,5,$data->idpelanggan,0,1);

$pdf->Cell(189,10,'',0,1);

$pdf->Cell(100,10,'Bill to :',0,1);

$pdf->Cell(10,5,'',0,0);
$pdf->Cell(90,5,$data->namapelanggan,0,1);

$pdf->Cell(10,5,'',0,0);
$pdf->Cell(90,5,$data->alamat,0,1);

$pdf->Cell(10,5,'',0,0);
$pdf->Cell(90,5,$data->nohp,0,1);

$pdf->Cell(10,5,'',0,0);
$pdf->Cell(90,5,'',0,1);
}
$pdf->Cell(189,10,'',0,1);

$pdf->SetFont('Arial','B',12);

$pdf->Cell(125,5,'Description',1,0);
$pdf->Cell(25,5,'Qty',1,0);
$pdf->Cell(39,5,'Prices',1,1);

$pdf->SetFont('Arial','',12);

$no = 1;
foreach ($ambil->result() as $data) {
									$pdf->Cell(125,5,$data->namamenu,1,0);
									$pdf->Cell(25,5,$data->jumlah,1,0);
									$pdf->Cell(39,5,$data->harga,1,1,'R');
								}	
	$no++;
									
	

	$pdf->Cell(125,5,'',0,0);
	$pdf->Cell(25,5,'Subtotal',0,0);
	$pdf->Cell(9,5,'Rp.',0,0);
	$pdf->Cell(30,5,$data->total,0,1,'R');

	$pdf->Cell(125,5,'',0,0);
	$pdf->Cell(25,5,'Cash In',0,0);
	$pdf->Cell(9,5,'Rp.',0,0);
	$pdf->Cell(30,5,$data->bayar,0,1,'R');

	$pdf->Cell(125,5,'',0,0);
	$pdf->Cell(25,5,'Cash Back',0,0);
	$pdf->Cell(9,5,'Rp.',0,0);
	$pdf->Cell(30,5,$data->kembali,0,1,'R');


	$pdf->Output("laporantrans.pdf","I");

?>