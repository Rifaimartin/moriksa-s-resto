<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">List of Transaction</h4>
                                <p class="category">Good day kopi, kopi Good day.</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="datatables" class="table table-hover table-striped">
                                    <thead>
                                    
                                        <th>#</th>
                                        <th>ID Transaction</th>
                                    	<th>ID Order</th>
                                        <th>Amount</th>
                                        <th>Cash In</th>
                                        <th>Cash Back</th>
                                        <th>Date</th>
                                        <td></td>
                                    	
                                    </thead>
                                    <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($ambil->result() as $data) {
                                    ?>
                                        <tr>

                                            <td><?php echo $no.'.'; ?></td>
                                            <td><?= $data->idtransaksi?></td>
                                            <td><?= $data->idpesanan?></td>
                                            <td>Rp. <?= $data->total?></td>
                                            <td>Rp. <?= $data->bayar?></td>
                                            <td>Rp. <?= $data->kembali?></td>
                                            <td><?= $data->tanggal?></td>
                                            <td>

                                            <a href="owner/cetak/<?php echo $data->idtransaksi; ?>" class="pe-7s-print btn btn-success btn-fill" > Print</a>
                                          
                                            </td>
                                    
                                        </tr>
                                        <?php
              $no++;
              } ?>
                        
                                    </tbody>
                 
                                </table>
                                
                            </div>
                        </div>
                    </div>
</div>
</div>
</div>