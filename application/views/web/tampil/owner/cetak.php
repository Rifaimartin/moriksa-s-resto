

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Create Account</h4>
                            </div>

                            <!-- Tampilan tambah akun -->
                            <div class="content">
                                <form action="owner/cetakfil" method="POST"> 
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Company</label>
                                                <input type="text" class="form-control" disabled placeholder="Company" value="Moriksa's Restaurant.">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>From</label>
                                                <input type="date" class="form-control" name="dari" id="dari" placeholder="e.g moriksa722" value="<?php echo date('Y-m-d'); ?>" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>End</label>
                                                <input type="date" class="form-control" name="end" id="end" value="<?php echo date('Y-m-d'); ?>" required="">
                                            </div>
                                        </div>
                                    </div>


                                    <button type="submit" class="btn btn-info btn-fill pull-right" name="submit" id="submit" value="Submit">Print Report</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            
                        </div>

                        
                </div>              
            </div>
            </div>
        </div>

