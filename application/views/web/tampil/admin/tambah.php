

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Create Account</h4>
                            </div>

                            <!-- Tampilan tambah akun -->
                            <div class="content">
                                <form action="" method="post"> 
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Company</label>
                                                <input type="text" class="form-control" disabled placeholder="Company" value="Moriksa's Restaurant.">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Username</label>
                                                <input type="text" class="form-control" required name="username" id="username" placeholder="e.g moriksa722" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control" required name="password" id="password">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Full Name</label>
                                                <input type="text" class="form-control" required name="nama" id="nama" placeholder="e.g Jodi Pascal">
                                            </div>
                                        </div>
                                        
                                    </div>

                                     <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Phone</label>
                                                <input type="phone" class="form-control" required name="telp" id="telp" placeholder="e.g 088934222">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" required name="email" id="email" placeholder="e.g monyet@antariksa.com">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Level User</label>
                                                <select name="lvl" id="lvl" class="form-control">
                                                
                                                <?php
                               foreach ($ambil->result() as $baris) { ?>
                              <option value="<?php echo $baris->idlevel; ?>" ><?php echo $baris->namalevel; ?></option>
                              <?php
                              } ?>
                        
                              </select>
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Address</label>
                                                <input type="text" name="alamat" id="alamat" class="form-control" required placeholder="e.g Bandung, Rancaekek">
                                            </div>
                                        </div>
                                    </div>


                                    <button type="submit" class="btn btn-info btn-fill pull-right" name="btn-tambah" id="btn-tambah">Create Account</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            
                        </div>

                        
                </div>              
            </div>
            </div>
        </div>

