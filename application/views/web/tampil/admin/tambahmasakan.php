

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><?= $judul; ?></h4>
                                
                            </div>

                            <!-- Tampilan tambah akun -->
                            <div class="content">
                            <form action="" method="post"> 

                                    <div class="row">

                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Company</label>
                                                <input type="text" class="form-control" disabled placeholder="Company" value="Moriksa's Restaurant.">
                                            </div>
                                        </div>

                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>ID Menu</label>
                                                <input type="text" class="form-control"  name="idmenu" id="idmenu" placeholder="e.g Cookies" value="<?php echo $kodeunik; ?>" readonly> 
                                                
                                            </div>
                                        </div>

                                    </div>

                                  

                                     <div class="row">

                                     <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control"  name="namamenu" id="namamenu" placeholder="e.g Cookies" value="">
                                                <label style="color:red" <?php echo form_error('namamenu'); ?> </label>
                                            </div>
                                        </div>

                
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Prices</label>
                                                <input type="number" class="form-control"  name="harga" id="harga" value="" placeholder="e.g 12000">
                                                <label style="color:red" <?php echo form_error('harga'); ?> </label>
                                            </div>
                                        </div>

                                    </div>

                                    <button type="submit" class="pe-7s-plus btn btn-success btn-fill pull-right" name="submit" id="submit" value="Submit"> Add Menu</button>
                                    <a href="web/tablemasak" class="pe-7s-cart btn btn-info btn-fill pull-left" > List Menu</a>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            
                        </div>

                        
                </div>              
            </div>
            </div>
        </div>

