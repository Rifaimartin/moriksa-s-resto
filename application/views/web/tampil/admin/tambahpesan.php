

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><?= $judul; ?></h4>
                                
                            </div>

                            <!-- Tampilan tambah akun -->
                            <div class="content">
                            <form action="" method="post"> 

                                    <div class="row">

                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Company</label>
                                                <input type="text" class="form-control" disabled placeholder="Company" value="Moriksa's Restaurant.">
                                            </div>
                                        </div>

                                         <div class="col-md-5">
                                            <div class="form-group">
                                                <label>ID Order</label>
                                                <input type="text" class="form-control"  name="idorder" id="idorder" placeholder="e.g Cookies" value="<?php echo $kodeunik; ?>" readonly> 
                                                
                                            </div>
                                        </div>

                                    </div>

                                  

                                     <div class="row">

                                     <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control"  name="namapelanggan" id="namapelanggan" placeholder="e.g Cookies" value="<?php echo $v->idpelanggan; ?>" readonly >
                                                <label style="color:red" <?php echo form_error('namapelanggan'); ?>> </label>
                                            </div>
                                        </div>

                         
                
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Menu</label>
                                                <select name="menu" id="menu" class="form-control" onchange='changeValue(this.value)' required>        
                        <?php 
                        $con = mysqli_connect("localhost", "root","", "restoran");
                            $result = mysqli_query($con, "SELECT * FROM menu");  
                            $jsArray = "var harga = new Array();\n";
                                while ($row = mysqli_fetch_array($result)) {  
                                echo '<option name="idmenu"  value="' . $row['idmenu'] . '">' . $row['namamenu'] . '</option>';  
                            $jsArray .= "harga['" . $row['idmenu'] . "'] = {harga:'".addslashes($row['harga'])."'};\n";
  }
  ?>
                          </select>
                                                </select>
                                            </div>
                                            <!-- <a href="web/add" class="pe-7s-plus btn btn-warning btn-fill" onclick="return confirm('Add  from menu?')"> Add Menu</a> -->
                                        </div>
                                        
                                    </div>

                                    <div class="row">

                                     <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Prices</label>
                                                <input type="text" class="form-control"  name="harga" id="harga" placeholder="e.g Cookies" value="" readonly>
                                                <label style="color:red" <?php echo form_error('harga'); ?> </label>
                                            </div>
                                        </div>

                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>qty</label>
                                                <input type="number" class="form-control"  name="quant" id="quant" placeholder="e.g Cookies" value="" required>
                                                <label style="color:red" <?php echo form_error('qty'); ?> </label>
                                            </div>
                                        </div>

                         
                
                                       
                                        
                                    </div>
                                    

                                    <div class="content table-responsive table-full-width">
                                <table id="" class="table table-hover table-striped">
                                    <thead>

                                        <th>#</th>
                                        <th>ID Menu</th>
                                        <th>ID Pelanggan</th>
                                        <th>Menu</th>
                                        <th>QTY</th>
                                        <th>Prices</th>
                                        <th>Options</th>
                                    	
                                    	
                                    </thead>
                                    <tbody>
                                    <?php $no = 1; ?>
                                    <?php foreach ($this->cart->contents() as $key): ?>
                                    <?php echo form_hidden($no.'[rowid]', $key['rowid']); ?>
                                    
                                        <tr>

                                            <td><?php echo $no.'.'; ?></td>
                                            <td><?= $key['id'];?></td>
                                            <td><?= $key['namapelanggan'];?></td>
                                            <td><?= $key['name'];?></td>
                                            <td><?= $key['qty'];?></td>
                                            <td style="text-align:left">Rp. <?= number_format($key['price'], 0, ',','.');?></td>
                                            <td>
                                            <a href="<?php echo base_url().'web/hapus/'.$key['rowid'];?>" class="pe-7s-trash btn btn-danger btn-fill" > Delete</a>
                                            </td>

                                            <div class="modal" id="<?= $key['rowid'];?>">
                                                <form action="<?php echo base_url();?>cart/update/<?= $key['rowid'];?> method="post">
                                                    <div class="modal-content">
                                                        <div class="input-field">
                                                            <input type="number" name="qty" id="qty" value="<?= $key['qty'];?>" id="qty<?= $key['rowid'];?>">
                                                            <label for="qty<?= $key['rowid'];?>"> Quantity</label>
                                                            </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                            <button type="submit" name="submit" value="Submit" class="modal-action btn blue"> Update order</button>
                                                            </div>
                                                            </form>
                                                            </div>
                                            
                                            
                                            
                                           
                                    
                                        </tr>
                                        <?php $no++; ?>
                                        <?php endforeach; ?>
                                        <tr>
                                            <td colspan="6">Total</td>
                                            <td colspan="1" style="text-align:right">Rp. <?= number_format($this->cart->total(),0,',','.');?></td>
                                            </tr>
                        
                                    </tbody>
                 
                                </table>
                                
                            </div>

                                    <button type="submit" class="pe-7s-plus btn btn-info btn-fill pull-right" name="submit" id="submit" value="Submit"> Add Order</button>
                                    <a href="web/simpan_pembelian/<?php echo $v->idpelanggan; ?>" class="pe-7s-cart btn btn-success btn-fill pull-left" onclick="return confirm('Are u sure thats all is done?')"> Check out</a>
                                    <div class="clearfix"></div>
                                </form>

<script type="text/javascript"> 
<?php echo $jsArray; ?>
function changeValue(id){
    document.getElementById('harga').value = harga[id].harga;
    

};
</script>

<script type="text/javascript"> 
$(".button-colapse").sideNav();
$(".modal").modal();
$(document).ready(function()
{
    $(window).scroll(function(){
        if ($(this).scrollTop() > 80){
            $('.back-top').fadeIn();
        } else {
            $('.back-top').fadeOut();
        }
    });
    $('back-top').click(function()
    {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});
</script>



                            </div>
                            
                        </div>

                        
                </div>              
            </div>
            </div>
        </div>

