<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">List of Clients</h4>
                                <p class="category">Good day kopi, kopi Good day.</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="datatables" class="table table-hover table-striped">
                                    <thead>

                                        <th>No</th>
                                        <th>Name</th>
                                    	<th>Gender</th>
                                    	<th>Phone</th>
                                        <th>Alamat</th>
                                        <th>Tanggal</th>
                                        <td></td>
                                    	
                                    </thead>
                                    <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($tpelanggan->result() as $data) {
                                    ?>
                                        <tr>

                                            <td><?php echo $no.'.'; ?></td>
                                            <td><?= $data->namapelanggan?></td>
                                            <td><?= $data->jeniskelamin?></td>
                                            <td><?= $data->nohp?></td>
                                            <td><?= $data->alamat?></td>
                                            <td><?= $data->tanggal?></td>
                                            <td>

                                            <a href="web/tambahpesanan/<?php echo $data->idpelanggan; ?>" class="pe-7s-cart btn btn-success btn-fill" > Order</a>
                                            <a href="web/hapuspelanggan/<?php echo $data->idpelanggan; ?>" class="pe-7s-trash btn btn-danger btn-fill" onclick="return confirm('Delete <?= $data->namapelanggan;?> from menu?')"> Hapus</a>
                                            </td>
                                    
                                        </tr>
                                        <?php
              $no++;
              } ?>
                        
                                    </tbody>
                 
                                </table>
                                
                            </div>
                        </div>
                    </div>
</div>
</div>
</div>