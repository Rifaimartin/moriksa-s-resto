

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><?= $judul; ?></h4>
                                
                            </div>

                            <!-- Tampilan tambah akun -->
                            <div class="content">
                            <form action="" method="post"> 

                                    <div class="row">

                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Company</label>
                                                <input type="text" class="form-control" disabled placeholder="Company" value="Moriksa's Restaurant.">
                                            </div>
                                        </div>

                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>ID Transaksi</label>
                                                <input type="text" class="form-control"  name="idtransaksi" id="idtransaksi" placeholder="e.g Cookies" value="<?php echo $ambil->idtransaksi; ?>" readonly> 
                                                
                                            </div>
                                        </div>

                                    </div>

                                  

                                     <div class="row">

                                     <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Total</label>
                                                <input type="text" class="form-control"  name="total" id="total" placeholder="e.g Cookies" value="<?php echo $ambil->total; ?>" readonly>
                                                <label style="color:red" <?php echo form_error('total'); ?> </label>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                     <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Bayar</label>
                                                <input type="number" class="form-control"  name="bayar" id="bayar" placeholder="" value="" onkeypress="return angka(event)" onFocus="mulaihitung()" onBlur="berhentihitung()">
                                                <label style="color:red" <?php echo form_error('bayar'); ?> </label>
                                            </div>
                                        </div>

                
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Kembali</label>
                                                <input type="number" class="form-control"  name="kembali" id="kembali" value="" placeholder="" readonly>
                                                <label style="color:red" <?php echo form_error('kembali'); ?> </label>
                                            </div>
                                        </div>

                                    </div>

                                    <button type="submit" class="pe-7s-plus btn btn-success btn-fill pull-right" name="submit" id="submit" value="Submit"> Add Transaction</button>
                                    <a href="web/tableorder" class="pe-7s-cart btn btn-info btn-fill pull-left" > List Order</a>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            
                        </div>
                        </script>

<script type="text/javascript">
function mulaihitung(){
  Interval= setInterval("hitung()",1);
}
function hitung(){

  total =parseInt(document.getElementById("total").value);
  bayar =parseInt(document.getElementById("bayar").value);
  kembali = bayar-total;
  document.getElementById("kembali").value = kembali;
}
function berhentihitung(){
  clearInterval(Interval);
}
</script>
                        
                </div>              
            </div>
            </div>
        </div>

