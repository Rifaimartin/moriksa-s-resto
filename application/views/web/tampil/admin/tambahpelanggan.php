

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><?= $judul; ?></h4>
                                
                            </div>

                            <!-- Tampilan tambah akun -->
                            <div class="content">
                            <form action="" method="post"> 

                                    <div class="row">

                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Company</label>
                                                <input type="text" class="form-control" disabled placeholder="Company" value="Moriksa's Restaurant.">
                                            </div>
                                        </div>

                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>ID Client</label>
                                                <input type="text" class="form-control"  name="idpelanggan" id="idpelanggan" placeholder="e.g Cookies" value="<?php echo $kodeunik; ?>" readonly> 
                                                
                                            </div>
                                        </div>

                                    </div>

                                  

                                     <div class="row">

                                     <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control"  name="namapelanggan" id="namapelanggan" placeholder="e.g Cookies" value="">
                                                <label style="color:red" <?php echo form_error('namapelanggan'); ?> </label>
                                            </div>
                                        </div>

                         
                
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Gender</label>
                                                <select name="jeniskelamin" id="jeniskelamin" class="form-control">
                                                <?php
                               foreach ($ambil->result() as $baris) { ?>
                              <option value="<?php echo $baris->idjenis; ?>" ><?php echo $baris->jeniskelamin; ?></option>
                                  <?php
                                     } ?>
                                                </select>
                                                <label style="color:red" <?php echo form_error('jeniskelamin'); ?> </label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                     <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Phone</label>
                                                <input type="number" class="form-control"  name="nohp" id="nohp" placeholder="e.g 089463354" value="">
                                                <label style="color:red" <?php echo form_error('nohp'); ?> </label>
                                            </div>
                                        </div>

                
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Adress</label>
                                                <input type="text" class="form-control"  name="alamat" id="alamat" value="" placeholder="e.g Bandung, Rancaekek">
                                                <label style="color:red" <?php echo form_error('alamat'); ?> </label>
                                            </div>
                                        </div>

                                    </div>

                                    <button type="submit" class="pe-7s-plus btn btn-success btn-fill pull-right" name="submit" id="submit" value="Submit"> Add Client</button>
                                    <a href="web/tablepelanggan" class="pe-7s-cart btn btn-info btn-fill pull-left" > List Clients</a>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            
                        </div>

                        
                </div>              
            </div>
            </div>
        </div>

