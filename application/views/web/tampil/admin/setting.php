

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Profile</h4>
                            </div>

                            <!-- Tampilan kiri edit profile -->
                            <div class="content">
                                <form action="" method="post"> 
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Company (disabled)</label>
                                                <input type="text" class="form-control" disabled placeholder="Company" value="Moriksa's Restaurant.">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Username</label>
                                                <input type="text" name="username" id="username" class="form-control" placeholder="" value="<?php echo $this->session->userdata('username@2019'); ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Password</label>
                                                <input type="password" required name="password" id="password" class="form-control" placeholder="" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Full Name</label>
                                                <input type="text" name="nama" id="nama" class="form-control" placeholder="" value="<?php echo $this->session->userdata('pemilik'); ?>">
                                            </div>
                                        </div>
            
                                    </div>

                                    <div class="row" hidden>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                            
                                                <label>Photo</label>
                                                <br>
                                                <div class="4"><img src="<?php echo $this->session->userdata('foto'); ?>">
                                                <br>
                                                <input type="file" name="foto" id="foto" class="form-control" placeholder="" value="">
                                            </div>
                                            </div>
                                        </div>
            
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Address</label>
                                                <input type="text" name="alamat" id="alamat" class="form-control" placeholder="" value="<?php echo $this->session->userdata('alamat'); ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Phone</label>
                                                <input type="text" name="telp" id="telp" class="form-control" placeholder="" value="<?php echo $this->session->userdata('telp'); ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address</label>
                                                <input type="email" name="email" id="email" class="form-control" placeholder="" value="<?php echo $this->session->userdata('email'); ?>">
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <button type="submit" class="btn btn-info btn-fill pull-right"  onclick="demo.showNotification('top','right')" name="btn-simpan" id="btn-simpan">Update Profile</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            
                        </div>

                        
                </div>
                <!-- Tampilan kanan profil -->
                 <div class="col-md-4">
                        <div class="card card-user">
                            <div class="image">
                            </div>
                            <div class="content">
                                <div class="author">
                                     <a>
                                    <img class="avatar border-gray" src="<?php echo $this->session->userdata('foto'); ?>" alt="..."/>

                                      <h4 class="title"><?php echo $this->session->userdata('pemilik'); ?><br/>
                                         <small><?php echo $this->session->userdata('username@2019'); ?></small>
                                      </h4>
                                    </a>
                                </div>
                                <p class="description text-center"> "<?php echo $this->session->userdata('telp'); ?> <br>
                                                    <?php echo $this->session->userdata('alamat'); ?> <br>
                                                    Moriksa's Restaurant."
                                </p>
                            </div>
                            <hr>
                           
            </div>
            </div>
            </div>
            </div>
        </div>

