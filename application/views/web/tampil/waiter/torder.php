<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">List of Order</h4>
                                <p class="category">Good day kopi, kopi Good day.</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="datatables" class="table table-hover table-striped">
                                    <thead>

                                        <th>#</th>
                                        <th>ID Order</th>
                                    	<th>Name Client</th>
                                        <th>Date</th>
                                        <td></td>
                                    	
                                    </thead>
                                    <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($torder->result() as $data) {
                                    ?>
                                        <tr>

                                            <td><?php echo $no.'.'; ?></td>
                                            <td><?= $data->idpesanan?></td>
                                            <td><?= $data->namapelanggan?></td>
                                            <td><?= $data->tanggal?></td>
                                            <td>

                                            
                                            <a href="waiter/hapuspelanggan/" class="pe-7s-trash btn btn-danger btn-fill" onclick="return confirm('Delete  from menu?')"> Hapus</a>
                                            </td>
                                    
                                        </tr>
                                        <?php
              $no++;
              } ?>
                        
                                    </tbody>
                 
                                </table>
                                
                            </div>
                        </div>
                    </div>
</div>
</div>
</div>