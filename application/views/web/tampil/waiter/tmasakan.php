<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">List of Menu</h4>
                                <p class="category">Good day kopi, kopi Good day.</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="datatables" class="table table-hover table-striped">
                                    <thead>

                                        <th>No</th>
                                        <th>Name</th>
                                    	<th>Prices</th>
                                    	<th>Option</th>
                                    	
                                    </thead>
                                    <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($tmasak->result() as $data) {
                                    ?>
                                        <tr>

                                            <td><?php echo $no.'.'; ?></td>
                                            <td><?= $data->namamenu?></td>
                                            <td><?= $data->harga?></td>
                                            <td>

                                            <a href="waiter/hapusmasak/<?php echo $data->idmenu; ?>" class="pe-7s-pen btn btn-warning btn-fill" onclick="return confirm('Edit <?= $data->namamenu;?> from menu?')"> Edit</a>
                                            <a href="waiter/hapusmasak/<?php echo $data->idmenu; ?>" class="pe-7s-junk btn btn-danger btn-fill" onclick="return confirm('Delete <?= $data->namamenu;?> from menu?')"> Hapus</a>
                                            </td>
                                    
                                        </tr>
                                        <?php
              $no++;
              } ?>
                        
                                    </tbody>
                 
                                </table>
                                
                            </div>
                        </div>
                    </div>
</div>
</div>
</div>