<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Owner extends CI_Controller {

	public function index()
	{
		$this->Model->cek_login();
		$this->load->view('web/tampil/owner/head');
		$this->load->view('web/tampil/owner/body');
		$this->load->view('web/tampil/owner/footer');
		}
	

	public function setting()
	{
		$this->Model->cek_login();
		$this->load->view('web/tampil/owner/head');
		$this->load->view('web/tampil/owner/setting');
		$this->load->view('web/tampil/owner/footert');

		if (isset($_POST['btn-simpan'])) 
					{
							$pm = $_POST['nama'];
							$nm = $_POST['username'];
							$pw = $_POST['password'];
							$alamat = $_POST['alamat'];
							$tlp = $_POST['telp'];
							$email = $_POST['email'];
							
					
					
    						$data = array(	
											'namauser'		=> $pm,
											'username'		=> $nm,
											'password'		=> md5($pw),
											'alamat'		=> $alamat,
											'nohp'			=> $tlp,
											'email'			=> $email
    									  	 );
    					
    										
											   $this->Model->update_data('user', array('username' => $ceks), $data);
											   $this->session->has_userdata('username@2019');
											   $this->session->set_userdata('username@2019', "$nm");
									
									echo "<script>alert('Success: Thanks for trusting Us!');history.go(-1);</script>";
    							
						}
		}
	

			public function tabletrans()
			{
				$this->Model->cek_login();
				
				$data['ambil']  	  = $this->db->query("SELECT DISTINCT 
				transaksi.idtransaksi, transaksi.idpesanan,transaksi.total, transaksi.bayar, transaksi.kembali, pelanggan.namapelanggan, transaksi.tanggal 
				FROM transaksi
				JOIN pesanan ON transaksi.idpesanan = pesanan.idpesanan
				JOIN pelanggan ON pelanggan.idpelanggan = pesanan.idpelanggan
				WHERE transaksi.tanggal = CURDATE()
				AND transaksi.status = 'DONE'
				
				");

				$this->load->view('web/tampil/owner/head', $data);
				$this->load->view('web/tampil/owner/ttransaksi', $data);
				$this->load->view('web/tampil/owner/footert');
			}

			public function cetak($id='')
			{
				$this->Model->cek_login();

				if ($id == '') {
					redirect('owner/ttransaksi');
				}
				
				$data['ambil']  	  = $this->db->query("SELECT 
				transaksi.idtransaksi, transaksi.idpesanan,transaksi.total, transaksi.bayar, transaksi.kembali, pelanggan.namapelanggan, transaksi.tanggal, pesanan.idmenu, pesanan.idpelanggan, pesanan.harga, pesanan.jumlah, menu.namamenu, pesanan.idpelanggan, pelanggan.nohp, pelanggan.alamat   
				FROM transaksi
				JOIN pesanan ON transaksi.idpesanan = pesanan.idpesanan
				JOIN menu ON menu.idmenu = pesanan.idmenu
				JOIN pelanggan ON pelanggan.idpelanggan = pesanan.idpelanggan
				WHERE transaksi.tanggal = CURDATE()
				AND transaksi.status = 'DONE'
				AND transaksi.idtransaksi = '$id'
				
				
				");
				$this->load->view('web/report/rep', $data);
			}
				
			public function cetakfil()
			{
				$this->Model->cek_login();

				$from=$_POST['dari'];
				$end=$_POST['end'];
				
				$data['ambil']  	  = $this->db->query("SELECT
					tanggal, total
				FROM transaksi
				WHERE (tanggal BETWEEN '$from' AND '$end')
				AND status = 'DONE'
				");

				$data['baru']  	  = $this->db->query("SELECT
				SUM(total) AS jumlahmasuk
				FROM transaksi
				WHERE (tanggal BETWEEN '$from' AND '$end')
				AND status = 'DONE'
			");

				
				
				$this->load->view('web/report/repf', $data);
			}

			public function cetakf()
			{
				$this->Model->cek_login();

				$data['judul']		= 'Print Report';

				//dari form
			
			$data['from'] = $this->input->get('dari', TRUE);
			$data['end'] = $this->input->get('end', TRUE);

				//validasi

			if($this->input->get('submit', TRUE) == 'Submit') {

				$this->form_validation->set_rules
				('dari', 'Start Date', 'required');

				$this->form_validation->set_rules
				('end', 'End Date', 'required');

				if($this->form_validation->run() == TRUE)
				{
					//proses insert 
					
					}
					$this->load->view('web/report/repf', $data);
					
				}

				$this->load->view('web/tampil/owner/head', $data);
				$this->load->view('web/tampil/owner/cetak', $data);
				$this->load->view('web/tampil/owner/footert');
			}
				
					
		
				
	}

