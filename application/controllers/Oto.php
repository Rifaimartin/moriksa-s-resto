<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oto extends CI_Controller {

		public function index() 
		{

		$this->load->view('web/login/head');
		$this->load->view('web/login/body');
			
    }
    
    public function login()
	{
		$data = array('username' => $this->input->post('username', TRUE),
						'password' => md5($this->input->post('password', TRUE))
			);
		$this->load->model('Model');
		$hasil = $this->Model->cek_user('user',$data); //ambil dari tabel user
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Loggin'; //membuat session "sudah login"
				$sess_data['username@2019'] = $sess->username; //membuat tampilan session sesuai username
				$sess_data['pemilik'] = $sess->namauser; //membuat tampilan session sesuai nama pemilik
				$sess_data['foto'] = $sess->foto; //membuat tampilan session sesuai foto pemilik
				$sess_data['alamat'] = $sess->alamat; //membuat tampilan session sesuai alamat
				$sess_data['email'] = $sess->email; //membuat tampilan session sesuai email
				$sess_data['password'] = $sess->password; //membuat tampilan session sesuai password
				$sess_data['telp'] = $sess->nohp; //membuat tampilan session sesuai notelp
				$sess_data['leveluser'] = $sess->leveluser; //membuat tampilan session sesuai leveluser
				$this->session->set_userdata($sess_data); //set session
			}
			if ($this->session->userdata('leveluser')=='1') {
				redirect('web');
			}
			elseif ($this->session->userdata('leveluser')=='2') {
				redirect('waiter');
			}		

			elseif ($this->session->userdata('leveluser')=='3') {
			redirect('kasir');
			}	

			elseif ($this->session->userdata('leveluser')=='4') {
			redirect('owner');
	}
		}
		else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
		}
	}



		public function logout() 
	{
     if ($this->session->has_userdata('username@2019')) 
     {
         $this->session->sess_destroy();
         redirect('web');
     }
     redirect('web');
    }
}