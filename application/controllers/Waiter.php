<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Waiter extends CI_Controller {

	public function index()
	{
		$this->Model->cek_login();
		$this->load->view('web/tampil/waiter/head');
		$this->load->view('web/tampil/waiter/body');
		$this->load->view('web/tampil/waiter/footer');
		}
	

	public function setting()
	{
		$this->Model->cek_login();
		$this->load->view('web/tampil/waiter/head');
		$this->load->view('web/tampil/waiter/setting');
		$this->load->view('web/tampil/waiter/footert');

		if (isset($_POST['btn-simpan'])) 
					{
							$pm = $_POST['nama'];
							$nm = $_POST['username'];
							$pw = $_POST['password'];
							$alamat = $_POST['alamat'];
							$tlp = $_POST['telp'];
							$email = $_POST['email'];
							
					
					
    						$data = array(	
											'namauser'		=> $pm,
											'username'		=> $nm,
											'password'		=> md5($pw),
											'alamat'		=> $alamat,
											'nohp'			=> $tlp,
											'email'			=> $email
    									  	 );
    					
    										
											   $this->Model->update_data('user', array('username' => $ceks), $data);
											   $this->session->has_userdata('username@2019');
											   $this->session->set_userdata('username@2019', "$nm");
									
									echo "<script>alert('Success: Thanks for trusting Us!');history.go(-1);</script>";
    							
						}
		}
	
			public function tambahmasak()
			{

			$this->Model->cek_login();
			$data['judul'] = "Add Menu";
			$data['kodeunik'] = $this->Model->code_menu();

			//dari form
			$data['namamenu'] = $this->input->post('namamenu', TRUE);
			$data['idmenu'] = $this->input->post('idmenu', TRUE);
			$data['harga'] = $this->input->post('harga', TRUE);

			//validasi

			if($this->input->post('submit', TRUE) == 'Submit') {

				$this->form_validation->set_rules
				('namamenu', 'Name', 'required|min_length[4]|is_unique[menu.namamenu]',
				array(
						'is_unique'     => 'This %s already exists.'
				)
				);
				
				$this->form_validation->set_rules('harga', 'Prices', 'required|numeric');

				if($this->form_validation->run() == TRUE)
				{

					//proses insert 
					$items = array(
						'idmenu'	=> $data['idmenu'],
						'namamenu' 	=> $data['namamenu'],
						
						'harga'		=> $data['harga']
					);
					$this->Model->save_data('menu', $items);
					redirect('waiter/tablemasak'); //redirect ke tabel
					

					}
					
				}
			
		

			$this->load->view('web/tampil/waiter/head', $data);
			$this->load->view('web/tampil/waiter/tambahmasakan',$data);
			$this->load->view('web/tampil/waiter/footert');

			}
			
			public function tablemasak()
			{
				$this->Model->cek_login();
				$data['tmasak'] = $this->Model->get_data('menu');
				$this->load->view('web/tampil/waiter/head', $data);
				$this->load->view('web/tampil/waiter/tmasakan', $data);
				$this->load->view('web/tampil/waiter/footert');
			}

			public function hapusmasak($id='')
			{
	
				$this->Model->cek_login();
		
				if ($id == '') {
				redirect('waiter/tambahmasak');
			}

			$cekr =	$this->Model->get_data_by_pk('menu', 'idmenu', $id)->num_rows();
					$this->Model->delete_data_by_pk('menu', 'idmenu', $id);
			
			redirect('waiter/tablemasak');

		
	}

	public function tambahclient()
			{

			$this->Model->cek_login();
			$data['judul'] = "Add Clients";
			$data['kodeunik'] = $this->Model->code_pelanggan();
			$data['ambil']	= $this->Model->get_data('jeniskelamin');

			//dari form
			$data['idpelanggan'] = $this->input->post('idpelanggan', TRUE);
			$data['namapelanggan'] = $this->input->post('namapelanggan', TRUE);
			$data['jeniskelamin'] = $this->input->post('jeniskelamin', TRUE);
			$data['nohp'] = $this->input->post('nohp', TRUE);
			$data['alamat'] = $this->input->post('alamat', TRUE);

			//validasi

			if($this->input->post('submit', TRUE) == 'Submit') {

				$this->form_validation->set_rules
				('namapelanggan', 'Name', 'required|min_length[4]');

				$this->form_validation->set_rules
				('jeniskelamin', 'Gender', 'required');

				$this->form_validation->set_rules
				('nohp', 'Phone', 'required|numeric|max_length[13]|min_length[10]');

				$this->form_validation->set_rules
				('alamat', 'Address', 'required');

				
				
				

				if($this->form_validation->run() == TRUE)
				{
					
					//proses insert 
					$items = array(
						'idpelanggan'		=> $data['idpelanggan'],
						'namapelanggan' 	=> $data['namapelanggan'],
						'jeniskelamin'		=> $data['jeniskelamin'],
						'alamat'			=> $data['alamat'],
						'tanggal'			=> date('Y-m-d'),
						'nohp'				=> $data['nohp']
					);
					$this->Model->save_data('pelanggan', $items);
					redirect('waiter/tablepelanggan'); //redirect ke tabel
					

					}
					
				}
			
		

			$this->load->view('web/tampil/waiter/head', $data);
			$this->load->view('web/tampil/waiter/tambahpelanggan',$data);
			$this->load->view('web/tampil/waiter/footert');

			}

			public function tablepelanggan()
			{
				$this->Model->cek_login();
				
				$data['tpelanggan']  	  = $this->db->query("SELECT * FROM pelanggan
				JOIN jeniskelamin ON pelanggan.jeniskelamin = jeniskelamin.idjenis WHERE pelanggan.tanggal = CURDATE()
				");

				//CURDATE() untuk tanggal
				//CURTIME() untuk waktu
				//NOW() untuk tanggal dan waktu
				$this->load->view('web/tampil/waiter/head', $data);
				$this->load->view('web/tampil/waiter/tpelanggan', $data);
				$this->load->view('web/tampil/waiter/footert');
			}

			public function hapuspelanggan($id='')
			{
	
				$this->Model->cek_login();
		
				if ($id == '') {
				redirect('waiter/tambahclient');
			}

			$cekr =	$this->Model->get_data_by_pk('pelanggan', 'idpelanggan', $id)->num_rows();
					$this->Model->delete_data_by_pk('pelanggan', 'idpelanggan', $id);
			
			redirect('waiter/tablepelanggan');

		
	}

			public function tambahpesanan($id='')
			{
				$this->Model->cek_login();
				
				

				if ($id == '') {
					redirect('waiter/tablepelanggan');
				}

				$data['judul']		= 'Add Order';
				$data['ambil'] 		= $this->Model->get_data('menu');
				$data['kodeunik'] 	= $this->Model->code_pesanan();
				$data['v']  	  = $this->db->query("SELECT * FROM pelanggan WHERE idpelanggan='$id'")->row();

				//dari form
			
			$data['namapelanggan'] = $this->input->post('namapelanggan', TRUE);
			$data['idorder'] = $this->input->post('idorder', TRUE);
			$data['menu'] = $this->input->post('menu', TRUE);
			$data['harga'] = $this->input->post('harga', TRUE);
			$data['quant'] = $this->input->post('quant', TRUE);
			
		
			//validasi

			if($this->input->post('submit', TRUE) == 'Submit') {

				$this->form_validation->set_rules
				('namapelanggan', 'Name', 'required|min_length[4]');

				$this->form_validation->set_rules
				('quant', 'Quantity', 'required|numeric|min_length[1]');

				$this->form_validation->set_rules
				('harga', 'Prices', 'required|numeric|min_length[2]');

				
				if($this->form_validation->run() == TRUE)
				{
				$komen=$this->input->post('menu');
				$produk=$this->Model->get_menu($komen);
				$i=$produk->row_array();
					//proses insert 
					$items = array(
						'id'       => $i['idmenu'],
						'name'     => $i['namamenu'],
						'price'    => $i['harga'],
						'idorder'  => $this->input->post('idorder'),
						'namapelanggan'  => $this->input->post('namapelanggan'),
						'qty'      => $this->input->post('quant')
					);
					$this->cart->insert($items);
					
					}
					
				}


				

				$this->load->view('web/tampil/waiter/head', $data);
				$this->load->view('web/tampil/waiter/tambahpesan', $data);
				$this->load->view('web/tampil/waiter/footert');
			}
			

			public function update($rowid){
				
				$this->form_validation->set_rules('qty','Quantity','required|numeric');
				
				if($this->form_validation->run() == TRUE)
				{
				$data = array(
					   'qty' => $this->input->post('qty', TRUE),
					   'rowid' => $rowid
					);
					$this->cart->update($data);
					echo "<script>alert('Sukses');history.go(2);</script>";
				}
			}

			public function hapus($rowid){
				
				
					$this->cart->update(array(
						   'rowid'      => $rowid,
						   'qty'     => 0
						));
						echo "<script>alert('Sukses');history.go(-2);</script>";
				
				}

				function simpan_pembelian($id=''){
					
					
					$ceks = $this->session->userdata('pemilik');
					$pelanggan = $this->input->post('namapelanggan', TRUE);
					$idorder = $this->Model->code_pesanan();
					$idmenu= $this->input->post('menu', TRUE);
					$harga = $this->input->post('harga', TRUE);
					$qty = $this->input->post('qty', TRUE);
					$trans = $this->Model->code_trans();
					$total = $this->cart->total();
					$bayar = 'Belum Bayar';

							$order_proses=$this->Model->simpan_pembelian($ceks,$pelanggan,$idorder,$idmenu,$harga,$qty,$trans,$total,$bayar);
							if($order_proses){
								$this->cart->destroy();
								echo $this->session->set_flashdata('msg','<label class="label label-success">Pembelian Berhasil di Simpan ke Database</label>');
								redirect('waiter/tableorder');	
							}else{
								redirect('waiter');
							}
					
					}

					public function tableorder()
			{
				$this->Model->cek_login();
				
				$data['torder']  	  = $this->db->query("SELECT DISTINCT 
				transaksi.idpesanan, pelanggan.namapelanggan, transaksi.tanggal 
				FROM transaksi
				JOIN pesanan ON transaksi.idpesanan = pesanan.idpesanan
				JOIN pelanggan ON pelanggan.idpelanggan = pesanan.idpelanggan
				WHERE transaksi.tanggal = CURDATE()
				AND transaksi.status = 'Belum Bayar'
				
				");

				$this->load->view('web/tampil/waiter/head', $data);
				$this->load->view('web/tampil/waiter/torder', $data);
				$this->load->view('web/tampil/waiter/footert');
			}

					
				
	}

