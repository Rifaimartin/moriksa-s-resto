<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends CI_Model {

	public function get_data($tbl)
	{
			$this->db->from($tbl);
			$post = $this->db->get();

			return $post;
	}

	public function get_data_by_pk($tbl, $where, $id)
	{
				$this->db->from($tbl);
				$this->db->where($where,$id);
				$query = $this->db->get();

				return $query;
	}

	public function get_data_join($tbl, $tbl2, $join)
	{
			$this->db->from($tbl);
			$this->db->join($tbl2, "$tbl2.$join=$tbl.$join");
			$post = $this->db->get();

			return $post;
	}

	public function get_data_join_by_pk($tbl, $tbl2, $join, $id)
	{
			$this->db->from($tbl);
			$this->db->join($tbl2, "$tbl2.$join=$tbl.$join");
			$query = $this->db->get();

			return $query;
	}

		public function get_data_join_order_limit($tbl, $tbl2, $join, $id, $order, $limit)
	{
			$this->db->from($tbl);
			$this->db->join($tbl2, "$tbl2.$join=$tbl.$join");
			$this->db->order_by("$tbl.$id", "$order");
			$this->db->limit($limit);
			$query = $this->db->get();

			return $query;
	}

	public function update_data($tbl, $where, $data)
	{
		$this->db->update($tbl, $data, $where);
		return $this->db->affected_rows();
	}

	public function save_data($tbl, $data)
	{
		$this->db->insert($tbl, $data);
		return $this->db->insert_id();
	}

	public function delete_data_by_pk($tbl, $where, $id)
	{
		$this->db->where($where, $id);
		$this->db->delete($tbl);
	}


		public function cek_user($tbl, $data) {
			$query = $this->db->get_where($tbl, $data);
			return $query;
        }
        
        function code_otomatis(){
            $this->db->select('RIGHT(dokter.kddokter,3) as kode ',false);
            $this->db->order_by('kddokter', 'desc');
            $this->db->limit(1);
            $query = $this->db->get('dokter');
            if($query->num_rows()<>0){
                $data = $query->row();
                $kode = intval($data->kode)+1;
            }else{
                $kode = 1;

            }
            $kodemax = str_pad($kode,3,"0",STR_PAD_LEFT);
            $kodejadid  = "D".$kodemax;
            return $kodejadid;

		}
		
		function cek_login(){
			$ceks = $this->session->userdata('username@2019');

		if(!isset($ceks)) 
		{
			redirect('oto');
		}
		}

		function code_menu(){
            $this->db->select('RIGHT(menu.idmenu,3) as kode ',false);
            $this->db->order_by('idmenu', 'desc');
            $this->db->limit(1);
            $query = $this->db->get('menu');
            if($query->num_rows()<>0){
                $data = $query->row();
                $kode = intval($data->kode)+1;
            }else{
                $kode = 1;

            }
            $kodemax = str_pad($kode,3,"0",STR_PAD_LEFT);
            $kodejadi  = "M".$kodemax;
            return $kodejadi;

		}

		function code_pelanggan(){
            $this->db->select('RIGHT(pelanggan.idpelanggan,3) as kode ',false);
            $this->db->order_by('idpelanggan', 'desc');
            $this->db->limit(1);
            $query = $this->db->get('pelanggan');
            if($query->num_rows()<>0){
                $data = $query->row();
                $kode = intval($data->kode)+1;
            }else{
                $kode = 1;

            }
            $kodemax = str_pad($kode,3,"0",STR_PAD_LEFT);
            $kodejadi  = "P".$kodemax;
            return $kodejadi;

		}
		
		function code_pesanan(){
            $this->db->select('RIGHT(pesanan.idpesanan,3) as kode ',false);
            $this->db->order_by('idpesanan', 'desc');
            $this->db->limit(1);
            $query = $this->db->get('pesanan');
            if($query->num_rows()<>0){
                $data = $query->row();
                $kode = intval($data->kode)+1;
            }else{
                $kode = 1;

            }
            $kodemax = str_pad($kode,3,"0",STR_PAD_LEFT);
            $kodejadi  = "O".$kodemax;
            return $kodejadi;

		}
		
		function get_menu($komen){
			$hsl=$this->db->query("SELECT * FROM menu where idmenu='$komen'");
			return $hsl;
		}
	
		function simpan_pembelian($ceks,$pelanggan,$idorder,$idmenu,$harga,$qty,$trans,$total,$bayar){


			$tgl  = date('Y-m-d H:i:s');
			$this->db->query("INSERT INTO transaksi (idtransaksi,idpesanan,total,bayar,tanggal,status) VALUES ('$trans','$idorder','$total','0','$tgl','$bayar')");
			foreach ($this->cart->contents() as $item) {
				$data=array(
					'idpesanan' 	=>	$item['idorder'],
					'idmenu'		=>	$item['id'],
					'harga'			=>	$item['price'],
					'idpelanggan'	=>	$item['namapelanggan'],
					'jumlah'		=>	$item['qty'],
					'iduser'		=>	$ceks
				);
				$this->db->insert('pesanan',$data);
				
			}
			return true;
		}

		function code_trans(){
            $this->db->select('RIGHT(transaksi.idtransaksi,3) as kode ',false);
            $this->db->order_by('idtransaksi', 'desc');
            $this->db->limit(1);
            $query = $this->db->get('transaksi');
            if($query->num_rows()<>0){
                $data = $query->row();
                $kode = intval($data->kode)+1;
            }else{
                $kode = 1;

            }
            $kodemax = str_pad($kode,3,"0",STR_PAD_LEFT);
            $kodejadi  = "T".$kodemax;
            return $kodejadi;

		}


}
